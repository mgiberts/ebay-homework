import {css} from 'styled-components'
/*
  Adrift in Dreams
  http://www.colourlovers.com/palette/580974/Adrift_in_Dreams
*/

const palette = {
  white: '#FFFFFF',
  sunlit_sea: '#CFF09E',
  sea_foaming: '#A8DBA8',
  sea_showing_green: '#79BD9A',
  there_we_could_sail: '#3B8686',
  drift_in_dreams: '#0B486B',
}

const mediaParams = {
  medium: '48em',
  large: '64em',
  desktop: '75em',
};

export default {
  primary: palette.there_we_could_sail,
  background: palette.white,
  primaryInverse: palette.white,
  backgroundInverse: palette.there_we_could_sail,
  secondary: palette.sea_foaming,
  media: {
    medium: (...args) => css`
      @media (min-width: ${mediaParams.medium}) {
        ${ css(...args) }
      }
    `,
    large: (...args) => css`
      @media (min-width: ${mediaParams.large}) {
        ${ css(...args) }
      }
    `,
    desktop: (...args) => css`
      @media (min-width: ${mediaParams.desktop}) {
        ${ css(...args) }
      }
    `
  },
}
