import styled, {css} from 'styled-components';

export const GridContainer = styled.div`
  display: flex;
  flex: 1 100%;
  align-items: stretch;
  box-sizing: border-box;
`

export const GridCell = styled.div`
  display: flex;
  flex: 1 0 auto;
  box-sizing: border-box;
`

export const Header = styled.h1`
  display: block;
  margin: 0;
  padding: 0;
  background: ${props => props.theme.primary};
  color: ${props => props.theme.primaryInverse};
  text-align: center;
  text-transform: uppercase;
  padding: 5px;
  font-size: 20px;
`;

export const Wrapper = styled.div`
  ${props => props.theme.media.desktop`
    width: 1200px;
    min-height: 100vh;
    margin: 0 auto;
  `}
`;

export const Container = GridContainer.extend``;

export const GalleryContainer = Container.extend`
  flex-wrap: wrap;
`;

export const Image = GridCell.extend`
  margin: 15px 0;
  background: url(${props => props.backgroundImage}) no-repeat center;
  background-size: contain;
  height: 100px;
  flex-basis:100%;
  border-bottom: 1px dotted ${props => props.theme.primary};

  ${props => props.fullscreen && css`
    margin: 0;
    width: auto;
    position: relative;
    left: 0;
    right: 0;
    justify-content: center;
    height: 400px;
    border-bottom: 0;
  `};



  ${props => props.theme.media.medium`
    background-position: left;
  `}
`;

export const Backdrop = styled.div`
  z-index: 10;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: black;
`;

export const ImageContainer = GridContainer.extend`
  ${props => props.fullscreen && css`
    display: block;
    z-index: 15;
    position: fixed;
    width: 100%;
  `}

  ${props => props.theme.media.medium`
    flex-basis: 25%;
    border-bottom: 0;
  `}
`;

export const ImageHeader = GridContainer.extend`
  color: ${props => props.theme.primaryInverse};
  max-width: 600px;
  flex: 0 1 600px;
  margin: 0 auto;
`;

export const ImageTitle = GridCell.extend`
  font-size: 20px;
  line-height: 20px;
  align-items: center;
`;
export const CloseButton = GridCell.withComponent('button').extend`
  color: ${props => props.theme.primaryInverse};
  width: 30px;
  height: 30px;
  border: 0;
  background: transparent;
  flex: 0 0 30px;
  font-size: 20px;
  line-height: 20px;
  align-items: center;
`;
