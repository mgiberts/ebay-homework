import React from 'react';
import Image from './GalleryImage';
import {GalleryContainer} from './styled';

export default ({images, title}) => (
  <GalleryContainer>
    {images && images.map(({uri}, key) => <Image key={`${uri}-${key}`} title={title} src={uri} />)}
  </GalleryContainer>
)
