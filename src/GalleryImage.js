import React from 'react';
import {
  Image,
  Backdrop,
  ImageHeader,
  ImageTitle,
  CloseButton,
  ImageContainer
} from './styled';

export default class GalleryImage extends React.Component {
  state = {
    fullscreen: false
  }

  viewFullScreen = (e) => {
    e.preventDefault();

    this.setState({fullscreen: true});
  }

  exitFullScreen = (e) => {
    e.preventDefault();

    this.setState({fullscreen: false});
  }

  getImageSrc = (src) => `https://${src}_${this.state.fullscreen ? '27': '2'}.jpg`

  render() {
    const {src, title} = this.props;
    const {fullscreen} = this.state;
    return (
      <React.Fragment>
        {fullscreen && <Backdrop />}
        <ImageContainer fullscreen={fullscreen}>
          {fullscreen && (
            <ImageHeader>
              <ImageTitle>
                {title}
              </ImageTitle>
              <CloseButton onClick={this.exitFullScreen}>
                x
              </CloseButton>
            </ImageHeader>
          )}
          <Image
            onClick={this.viewFullScreen}
            fullscreen={fullscreen}
            backgroundImage={this.getImageSrc(src)}
          />
        </ImageContainer>
      </React.Fragment>
    );
  }
}
