import React from 'react';
import {
  Wrapper,
  Container,
  Header
} from './styled';

export default ({children}) => (
  <Wrapper>
    <Header>
      Car Photos
    </Header>
    <Container>
      {children}
    </Container>
  </Wrapper>
)
