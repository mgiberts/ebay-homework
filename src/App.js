import React from 'react';
import {ThemeProvider, injectGlobal} from 'styled-components';
import {Get} from 'react-axios';
import {isNull} from 'util';
import theme from './theme';
import Layout from './Layout';
import Loading from './Loading';
import Error from './Error';
import Gallery from './Gallery';

injectGlobal`
  body {
    font-family: sans-serif;
    margin: 0;
    padding: 0;
  }
`;

export default () => (
  <ThemeProvider theme={theme}>
    <Layout>
      <Get url="/data.json">
        {(error, response, isLoading) => {
          if (isLoading || isNull(response)) {
            return <Loading />
          }
          if (error || !response.data) {
            return <Error />
          }

          return <Gallery title={response.data.title} images={response.data.images} />;
        }}
      </Get>
    </Layout>
  </ThemeProvider>
);
