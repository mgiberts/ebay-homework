import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Gallery from '../Gallery';
import GalleryImage from '../GalleryImage';

Enzyme.configure({
  adapter: new Adapter()
})

const imagesStub = [
  {uri: 'test-uri-1'},
  {uri: 'test-uri-2'}
]

describe('<Gallery /> should', () => {
  it('render without props', () => {
    const Component = Enzyme.shallow(<Gallery />);
    expect(Component.length).toBe(1);
  });

  it('render without crashing', () => {
    const Component = Enzyme.shallow(<Gallery images={imagesStub} title={'title'} />);

    expect(Component.find(GalleryImage).length).toBe(2);
    expect(Component.length).toBe(1);
  });
});
