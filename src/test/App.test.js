import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';

Enzyme.configure({
  adapter: new Adapter()
})

describe('<App /> should', () => {
  it('render without crashing', () => {
    const Component = Enzyme.shallow(<App />);
    expect(Component.length).toBe(1);
  });
});
