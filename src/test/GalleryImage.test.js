import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GalleryImage from '../GalleryImage';

Enzyme.configure({
  adapter: new Adapter()
})

describe('<GalleryImage /> should', () => {
  it('render without props', () => {
    const Component = Enzyme.shallow(<GalleryImage />);
    expect(Component.length).toBe(1);
  });

  it('match snapshot', () => {
    const Component = Enzyme.shallow(<GalleryImage src={'test-uri-1'} title={'test-title'} />);
    expect(Component).toMatchSnapshot();
  });
});
